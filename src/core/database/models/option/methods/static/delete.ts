import {OptionDocument} from "../../interfaces";
import {Model} from "mongoose";

export async function deleteFn(this: Model<OptionDocument>, option: string): Promise<void> {
  await this.findOneAndDelete()
    .where("name").equals(option)
    .exec();
}
