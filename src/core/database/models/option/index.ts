import {Connection} from "mongoose";
import {optionSchema} from "./schema";
import debug from "debug";
import {OptionDocument, OptionModel} from "./interfaces";

const log = debug("zifar").extend("database");

export function getOptionModel(connection: Connection): OptionModel {
  return connection.model<OptionDocument, OptionModel>("Option");
}

export function registerOptionModel(connection: Connection): void {
  log("Building Option model.");

  connection.model<OptionDocument>("Option", optionSchema);
}
