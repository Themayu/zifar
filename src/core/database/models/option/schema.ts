import {Schema} from "mongoose";
import debug from "debug";
import {OptionDocument} from "./interfaces";

const log = debug("zifar").extend("database");

log("Building Option schema.");

export const optionSchema = new Schema<OptionDocument>({
  name: {
    type: String,
    required: true,
    unique: true
  },

  value: {
    type: Object,
    default: null
  }
});

log("- Adding static methods...");

optionSchema.static("get", require("./methods/static/get").getFn);
log("  - method 'get' added.");

optionSchema.static("delete", require("./methods/static/delete").deleteFn);
log("  - method 'delete' added.");

optionSchema.static("set", require("./methods/static/set").setFn);
log("  - method 'set' added.");
