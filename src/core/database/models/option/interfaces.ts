import {Document, Model} from "mongoose";

export interface OptionDocument extends Document {
  readonly name: string;
  value?: any;
}

export interface OptionModel extends Model<OptionDocument> {
  /**
   * Deletes an option's value from the collection.
   *
   * @param option The option to remove.
   */
  delete(option: string): Promise<void>;

  /**
   * @description Gets an option's value by name, returning a fallback value if it is not set.
   *
   * @param option The option to retrieve.
   * @param fallback Fallback value to use if the option is not set.
   */
  get<T>(option: string, fallback: T): Promise<T>;

  /**
   * Inserts or updates an option in the collection by name.
   *
   * @param option The option to alter.
   * @param value The value to store in the option.
   *
   * @returns the value that was stored.
   */
  set<T>(option: string, value: T): Promise<T>;
}
