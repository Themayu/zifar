import {Request, Response, NextFunction} from "express-serve-static-core"

export function setBundles(req: Request, res: Response, next: NextFunction) {
  res.locals.scriptBundles = Array("global").concat(req.app.get("feature.bundles"));

  next();
}

export function index(req: Request, res: Response): void {
  res.render("index");
}
