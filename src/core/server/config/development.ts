import * as debug from "debug";
import {Application} from "express-serve-static-core";
import {resolve} from "path";
import hbsModule = require("hbs");
import hbsUtils = require("hbs-utils");

export function apply(
  app: Application,
  engine: ReturnType<typeof hbsModule.create>,
  utils: ReturnType<typeof hbsUtils>
) {
  const log = debug("zifar").extend("http");
  log(`Applying development environment settings.`);

  app.engine("hbs", engine.__express);

  app.set("cache-control", false);
  app.set("cache-max-age", 0);
  app.set("gzip-threshold", 0);
  app.set("view engine", "hbs");
  app.set("view options", {layout: "_layout"});
  app.set("x-powered-by", false);

  let partials = 0;
  // noinspection CommaExpressionJS
  utils.registerWatchedPartials(resolve(__dirname, "../engine/partials"), {
    match: /\.hbs$/,
    name: template => { log(`Registering partial: ${template}`); partials++; return template },
    onchange: template => log(`Partial modified: ${template}`)
  }, () => log(`Registered ${partials} partials.`));
}
