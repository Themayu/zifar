import registerHtmlScriptTagsHelper from "./html/script_tags"
import registerHtmlStylesheetTagsHelper from "./html/stylesheet_tags";
import hbsModule = require("hbs");

export function registerHelpers(hbs: ReturnType<typeof hbsModule.create>) {
  registerHtmlScriptTagsHelper(hbs);
  registerHtmlStylesheetTagsHelper(hbs);
}
