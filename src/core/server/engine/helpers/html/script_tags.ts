import {SafeString} from "handlebars";
import {ViewContext} from "../types";
import hbsModule = require("hbs");

export default function registerHelper(hbs: ReturnType<typeof hbsModule.create>) {
  hbs.registerHelper("script_tags", function (this: ViewContext) {
    return new SafeString(this._locals.scriptBundles
      .map(script => `<script href="/static/css/${script.endsWith("js") ? script.slice(0, -3) : script}.js"></script>`)
      .join("\n"));
  });
}
