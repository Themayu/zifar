import {SafeString} from "handlebars";
import {ViewContext} from "../types";
import hbsModule = require("hbs");

export default function registerHelper(hbs: ReturnType<typeof hbsModule.create>) {
  hbs.registerHelper("stylesheet_tags", function (this: ViewContext) {
    return new SafeString(this._locals.stylesheets
      .map(sheet => `<link rel="stylesheet" href="/static/css/${sheet.endsWith("css") ? sheet.slice(0, -4) : sheet}.css" />`)
      .join("\n"));
  });
}
