// @TODO: Move this file into its own package.
export type List<T> = ListNode<T> | EmptyNode;

type ListDiff<T> = [List<T>, List<T>];
type ListNode<T> = Readonly<{head: T; tail: ListNode<T>}>;
type EmptyNode = ListNode<undefined>;

const EmptyNode: EmptyNode = List(undefined);

export function List<T>(...values: T[]): List<T> {
  if (values.length === 0) {
    return EmptyNode;
  }

  return Array.from(new Set(values))
    .map(value => ({head: value} as ListNode<T>))
    .reduceRight((previousValue, currentValue) => ({head: currentValue.head, tail: previousValue}));
}

export function clone<T>(list: List<T>): List<T> {
  return List(..._toArray(list));
}

export function concat<T>(list: List<T>, ...others: List<T>[]): List<T> {
  return others.reduce(_concat2, list);
}

export function filter<T>(list: List<T>, predicate: (this: List<T>, value: T, index: number) => boolean): List<T> {
  const newList: T[] = [];

  let item = list;
  for (let index = 0; !isEmpty(item); index++, item = _tail(item)) {
    const result = predicate.call(list, head(item), index);

    if (result) {
      newList.push(head(item));
    }
  }

  return List(...newList);
}

export function get<T>(list: List<T>, index: number): T | undefined {
  if (index === 0) {
    return head(list);
  }

  return get(_tail(list), index -1);
}

export function head<T>(node: ListNode<T>): T {
  return node.head;
}

export function has<T>(list: List<T>, value: T): boolean {
  if (head(list) === value) {
    return true;
  }

  return !isEmpty(_tail(list))
    ? has(_tail(list), value)
    : false;
}

export function insert<T>(list: List<T>, ...values: T[]): List<T> {
  return concat(list, List(...values));
}

export function insertBefore<T>(list: List<T>, ...values: T[]): List<T> {
  return concat(List(...values), list);
}

export function isEmpty(list: List<any>): list is EmptyNode {
  return list === EmptyNode;
}

export function length(list: List<any>): number {
  return !isEmpty(list)
    ? 1 + length(_tail(list))
    : 0;
}

export function removeAt<T>(list: List<T>, index: number, deleteCount: number) {
  return splice(list, index, deleteCount);
}

export function slice<T>(list: List<T>, start: number = 0, end: number = Number.POSITIVE_INFINITY): List<T> {
  const len = length(list);

  start = (start < 0) ? Math.max(len + start, 0) : Math.min(len, start);
  end = (end < 0) ? Math.min(len + end, len) : Math.min(len, end);

  return filter(list, (value, index) => index >= start && index < end);
}

export function splice<T>(list: List<T>, start: number, deleteCount: number, ...items: T[]): ListDiff<T> {
  let arr = _toArray(list);
  let diff = arr.splice(start, deleteCount, ...items);

  return [
    List(...arr),
    List(...diff)
  ];
}

export function tap<T>(list: List<T>, procedure: (list: List<T>) => void) {
  procedure(list);

  return list;
}

function _concat2<T>(list: List<T>, other: List<T>): List<T> {
  return splice(list, Infinity, 0, ..._toArray(other))[0];
}

function _tail<T>(node: ListNode<T>): List<T> {
  return typeof node.tail !== "undefined"
    ? node.tail
    : EmptyNode;
}

function _toArray<T>(list: List<T>): T[] {
  const items: T[] = [];

  for (let item = list; !isEmpty(item); item = _tail(item)) {
    items.push(head(item));
  }

  return items;
}

