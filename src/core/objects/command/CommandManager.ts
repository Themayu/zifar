import {Message} from "discord.js";
import debug from "debug";
import {promises as fs} from "fs";
import {Connection} from "mongoose";
import {CommandRegistry} from "./CommandRegistry";
import {DiscordConnection} from "../../bot";
import {getOptionModel} from "../../database/models/option";
import {join} from "path";

type CommandInfo = {
  found: true,
  commandId: string,
  args?: string
}

type CommandNotFound = {
  found: false
}

type FindResult = CommandInfo | CommandNotFound;

export class CommandManager {
  // noinspection HtmlDeprecatedTag
  static commandRegex = "(?<command>[^ ]+)(?: (?<args>[^]+))?";

  static async findCommand(invocationStr: string, connection: Connection): Promise<FindResult> {
    const Option = getOptionModel(connection);
    const prefix = await Option.get("prefix", "!");

    let match: RegExpMatchArray | null;

    // noinspection JSAssignmentUsedAsCondition
    if (match = invocationStr.match(prefix + CommandManager.commandRegex)) {
      return { found: true, commandId: match.groups!["command"], args: match.groups!["args"] };
    } else {
      return { found: false };
    }
  }

  constructor(protected registry: CommandRegistry, private connection: DiscordConnection) {
  }

  execute(connection: Connection): (message: Message) => void {
    return (message: Message) => {
      CommandManager.findCommand(message.content, connection)
        .then(result => result.found ? this.runCommand(result.commandId, message, result.args) : undefined);
    }
  }

  async loadCommands(directory: string): Promise<number> {
    const log = debug("zifar");

    const list = (await fs.readdir(directory)).map(listing => join(directory, listing));
    const stats = await Promise.all(list.map(path => fs.lstat(path)));
    const files = list.filter((path, index) => stats[index].isFile() && path.endsWith(".js"));
    const allCommands = files.map(file => require(file).command);
    const topLevelCommands = allCommands.filter(command => !command.isSubcommand());

    topLevelCommands.forEach(command => {
      this.registry.register(command);

      log(`bot: - loaded command '${command.name}'`);
    });

    return topLevelCommands.length;
  }

  runCommand(commandId: string, message: Message, strArgs?: string): void {
    this.registry.run(commandId, {...this.connection, message}, strArgs);
  }
}
