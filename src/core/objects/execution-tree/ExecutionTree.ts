import {TreeNode} from "./TreeNode";
import {CallArgs, CallInfo} from "../command/Command";

export class ExecutionTree {
  private constructor(public root: TreeNode) {}

  static create(root: TreeNode): ExecutionTree {
    return new this(root);
  }

  run(info: CallInfo, args: CallArgs, value: any): void {
    this.root.run(info, args, value);
  }
}
