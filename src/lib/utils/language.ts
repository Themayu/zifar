import {CallArgs} from "../../core/objects/command/Command";

const rest = Symbol.for("parseStr.rest");

/**
 * @description Determine whether a value is considered empty. A value is considered empty if it is equal to `null` or
 * `undefined`. Cannot operate on nonexistent variables.
 *
 * @param value Value to be checked.
 */
export function isNil(value: any): value is (null | undefined) {
  return (value === null || typeof value === "undefined");
}

/**
 * @description Parse an argument string, transforming it into an argument map. Any uncaught values are stored as the
 * symbol `[@@parseStr.rest]`.
 *
 * @param keys The list of values to extract from the argument string.
 * @param argStr The argument string to parse.
 *
 * @returns a map of the parsed arguments.
 */
export function parseStr(keys: Array<string>, argStr: string): CallArgs {
  let input: string[] = [...argStr];
  let out: CallArgs = new Map<string|symbol, string>();

  function getNextChunk(input: string[]): string {
    enum ParseFlags {
      FIND_WORD,
      FIND_DBL_CHUNK,
      FIND_SGL_CHUNK
    }

    let character;
    let flag = ParseFlags.FIND_WORD;
    let output = "";

    // noinspection JSAssignmentUsedAsCondition
    while (character = input.shift()) {
      if (character === '"') {
        if (flag === ParseFlags.FIND_WORD) {
          flag = ParseFlags.FIND_DBL_CHUNK;
        } else if (flag === ParseFlags.FIND_DBL_CHUNK && !output.endsWith("\\")) {
          flag = ParseFlags.FIND_WORD;
        } else {
          if (flag === ParseFlags.FIND_DBL_CHUNK && output.endsWith("\\")) {
            output = output.slice(0, -1);
          }

          output += character
        }
      } else if (character === "'") {
        if (flag === ParseFlags.FIND_WORD) {
          flag = ParseFlags.FIND_SGL_CHUNK;
        } else if (flag === ParseFlags.FIND_SGL_CHUNK && !output.endsWith("\\")) {
          flag = ParseFlags.FIND_WORD;
        } else {
          if (flag === ParseFlags.FIND_SGL_CHUNK && output.endsWith("\\")) {
            output = output.slice(0, -1);
          }

          output += character
        }
      } else if (character === " ") {
        if (flag === ParseFlags.FIND_WORD) {
          break;
        } else {
          output += character;
        }
      } else {
        output += character;
      }
    }

    return output;
  }

  for (let key of keys) {
    out.set(key, getNextChunk(input));
  }

  return out.set(rest, input.join(""));
}
