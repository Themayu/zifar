/**
 * Constrain a number to a given minimum and maximum value.
 *
 * @param min
 * @param max
 * @param value
 */
export function constrain(min: number, max: number, value: number): number {
  return Math.min(Math.max(value, min), max);
}
