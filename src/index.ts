import debug from "debug";
import {getDiscordConnection} from "./core/bot";
import {getDatabaseConnection} from "./core/database";
import {onPort} from "./core/server";

async function start(): Promise<void> {
  const log = debug("zifar").extend("app");

  log("Starting application.");

  const     {db} = await getDatabaseConnection();
  const {client} = await getDiscordConnection({db});
  const {server} = await (onPort(8746).getWebserver({db, client}));

  log("Application booted successfully.");
}

start();
