declare module "hbs-utils" {
  import * as baseModule from "hbs";

  interface RegisterPartialsOptions {
    // A regular expression that each partial's filename is tested against to determine whether it is a valid partial.
    // Default: <code>/\.(html|hbs)$/</code>
    match?: (string | RegExp);

    // A callback that can be used to change the name that a partial is registered under. Receives the original name of
    // the partial, and returns the new name to register it under.
    name?: (template: string) => string;

    // A callback that is called whenever a partial is added or updated. Receives the name of the partial that was changed.
    onchange?: (template: string) => void;

    // If <code>true</code>, the partials will be precompiled when they are registered.
    // Default: <code>false</code>
    precompile?: boolean;
  }

  class Instance {
    // Precompile all partials that are registered but haven't been compiled yet.
    precompilePartials(): void;

    // Perform a one-time registration of all *.html and *.hbs files in a directory as Handlebars partials.
    registerPartials(directory: string, opts?: RegisterPartialsOptions, done?: () => void): void;

    // Watch a directory for updates, registering all *.html and *.hbs files as Handlebars partials on change.
    registerWatchedPartials(directory: string, opts?: RegisterPartialsOptions, done?: () => void): void;
  }

  function create(hbs: ReturnType<typeof baseModule.create>): Instance;

  export = create;
}
